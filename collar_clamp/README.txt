Collar clamp

Half-ring design for fast usage.
Supports clamping on up to D16 mm tubes by default.

Requires:
M5 nut DIN934
M5 bolt for fastening

Original purpose is for maintaining position of telescopic elements, like music stand.

License:
https://creativecommons.org/licenses/by/4.0/
