$fn = 100;
$t = 0;
d_inner = 16+2*0.2;
H = 12;

nuthole_h = 4.4;
D = d_inner+2*5;
D2 = 1.1*D;
k = (d_inner)/16.4;
d_off = 2*k;
D_off = (D2-D)/2;
tol = 0.01;

show_bolts = false;
//include <bolts/BOLTS.scad>;


if (show_bolts) {
translate([d_inner/2+16-2,0,0])
rotate([0,-90,0])
DIN933("M5",16);
}

if (show_bolts) {
translate([d_inner/2+d_off+tol,0,0])
rotate([0,90,0])
DIN934("M5");
}

module collar_clamp() {
difference() {

  union() {
    translate([D_off,0,0])
    scale([D2/D,1,1])
    cylinder(h=H,d=D,center=true);

    translate([d_inner/4+d_off+nuthole_h+2,0,0])
    rotate([90,0,0])
    rotate([0,90,0])
    cylinder(h=d_inner/2,d=H*1.155,$fn=6,center=true);
  }

  cylinder(h=H+2*tol,d=d_inner,center=true);

  translate([-0.5-d_inner/4,0,0])
  cube([d_inner/2,5,H+2*tol],center=true);

  hull() {
    difference() {
      cylinder(h=H+2*tol,d=d_inner,center=true);
      translate([-D/2,0,0])
      cube([D,D,H+2*tol],center=true);
    }
    translate([d_off+d_inner/4,-D/2,0])
    cube([d_inner/2,D,H+2*tol],center=true);
  }

  translate([d_off,-D/2,0])
  cube([d_inner,D,H+2*tol],center=true);

  translate([0,-D-D/3,0])
  cube([D*2,D*2,H+2*tol],center=true);

  translate([nuthole_h+d_off,0,0])
  rotate([90,0,0])
  rotate([0,90,0])
  union() {
    cylinder(h=d_inner/2,d=9.4,$fn=6);
    cylinder(h=D,d=5.5);
    cylinder(h=D,d=6.4,$fn=6);
  }


  if (false) {//show nut section
    translate([0,0,H/2])
    cube([2*D,2*D,H],center=true);
  }

  if (true) {
    translate([0,0,H-tol])
    cube([2*D,2*D,H],center=true);
    translate([0,0,-H+tol])
    cube([2*D,2*D,H],center=true);
  }

}
}

collar_clamp();

if (false) {//show test tube
color([1,0,0,0.8])
translate([1.4*$t*k,-2*$t*k,0])
cylinder(h=H,d=(d_inner-0.2)*1+10*0,center=true);
}
