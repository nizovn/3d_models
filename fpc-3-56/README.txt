Fiber polarization controller fpc-3-56
for 3 mm fiber and 56 mm paddle loop diameter

Based on original customizable model by Filip Sośnicki:
https://www.printables.com/model/268294-fiber-polarization-controller

Requires:
8 screws M4x12 DIN912
8 nuts M4 DIN934

License:
https://creativecommons.org/licenses/by/4.0/
